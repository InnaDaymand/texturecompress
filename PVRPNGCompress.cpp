#include <iostream>
#include <png.h>
#include <pngconf.h>
#include "Bitmap.h"
#include "RgbBitmap.h"
#include "RgbaBitmap.h"
#include "PvrTcEncoder.h"
#include "PvrTcDecoder.h"
using namespace Javelin;

void abort_(const char * s, ...)
{
        va_list args;
        va_start(args, s);
        vfprintf(stderr, s, args);
        fprintf(stderr, "\n");
        va_end(args);
        abort();
}



Bitmap* read_png_file(char* file_name)
{
        unsigned char header[8];    // 8 is the maximum size that can be checked

        png_structp png_ptr;
        png_infop info_ptr;

        /* open file and test for it being a png */
        FILE *fp = fopen(file_name, "rb");
        if (!fp)
                abort_("[read_png_file] File %s could not be opened for reading", file_name);
        fread(header, 1, 8, fp);
        if (png_sig_cmp(header, 0, 8))
                abort_("[read_png_file] File %s is not recognized as a PNG file", file_name);


        /* initialize stuff */
        png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);

        if (!png_ptr)
                abort_("[read_png_file] png_create_read_struct failed");

        info_ptr = png_create_info_struct(png_ptr);
        if (!info_ptr)
                abort_("[read_png_file] png_create_info_struct failed");

        if (setjmp(png_jmpbuf(png_ptr)))
                abort_("[read_png_file] Error during init_io");

        png_init_io(png_ptr, fp);
        png_set_sig_bytes(png_ptr, 8);

        png_read_info(png_ptr, info_ptr);

        int width = png_get_image_width(png_ptr, info_ptr);
        int height = png_get_image_height(png_ptr, info_ptr);
        png_byte color_type = png_get_color_type(png_ptr, info_ptr);
        png_byte bit_depth = png_get_bit_depth(png_ptr, info_ptr);

        Bitmap *bitmap = NULL;
        if(color_type==2){ //RGB
            RgbBitmap *rgb = new RgbBitmap(width, height);
            bitmap=rgb;
        }
        if(color_type==6){ //RGBA
            RgbaBitmap *rgba = new RgbaBitmap(width, height);
            bitmap=rgba;
        }

        bitmap->color_type=color_type;
        bitmap->bit_depth=bit_depth;
        bitmap->number_of_passes= png_set_interlace_handling(png_ptr);
        png_read_update_info(png_ptr, info_ptr);


        /* read file */
        if (setjmp(png_jmpbuf(png_ptr)))
                abort_("[read_png_file] Error during read_image");
//        int bytePerPixel=png_get_rowbytes(png_ptr,info_ptr)/width;
        int rowbytes = png_get_rowbytes(png_ptr, info_ptr);
        png_bytep  row_pointers[height];
        for (int y=0; y<height; y++)
                row_pointers[y] = bitmap->data + y*rowbytes;

        png_read_image(png_ptr, row_pointers);

        fclose(fp);
        return bitmap;
}

void write_png_file(char *filename, Bitmap* bitmap) {

  FILE *fp = fopen(filename, "wb");
  if(!fp) abort();

  png_structp png = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
  if (!png) abort();

  png_infop info = png_create_info_struct(png);
  if (!info) abort();

  if (setjmp(png_jmpbuf(png))) abort();

  png_init_io(png, fp);

  // Output is 8bit depth, RGBA format.
  png_set_IHDR(
    png,
    info,
    bitmap->width, bitmap->height,
    8,
    bitmap->color_type,
    PNG_INTERLACE_NONE,
    PNG_COMPRESSION_TYPE_DEFAULT,
    PNG_FILTER_TYPE_DEFAULT
  );
  png_write_info(png, info);

  // To remove the alpha channel for PNG_COLOR_TYPE_RGB format,
  // Use png_set_filler().
  //png_set_filler(png, 0, PNG_FILLER_AFTER);

  if (!bitmap->data) abort();
  png_bytep  row_pointers[bitmap->height];
  for (int y=0; y<bitmap->height; y++)
          row_pointers[y] = bitmap->data + y*bitmap->width*bitmap->bytesPerPixel;

  png_write_image(png, row_pointers);
  png_write_end(png, NULL);

  fclose(fp);

  png_destroy_write_struct(&png, &info);
}


int main(int argc, char **argv) {
	if(argc ==3){
		std::string file_name_read=argv[1];
		std::string file_name_write=argv[2];
		int pos=file_name_read.find("'", 0);
		while(pos>=0){
			file_name_read.replace(pos, 1, "");
			pos=file_name_read.find("'", 0);
		}
		pos=file_name_write.find("'", 0);
		while(pos>=0){
			file_name_write.replace(pos, 1, "");
			pos=file_name_write.find("'", 0);
		}
		Bitmap* bitmap=read_png_file(((char*)file_name_read.data()));
        bool isRgb = dynamic_cast<RgbBitmap *>(bitmap) != NULL;

        const int size = bitmap->GetArea() / 2;
        unsigned char *pvrtc = new unsigned char[size];
        memset(pvrtc, 0, size);
        if (isRgb) {
            RgbBitmap *rgb = static_cast<RgbBitmap *>(bitmap);
            ColorRgb<unsigned char> *data = rgb->GetData();
            PvrTcEncoder::EncodeRgb4Bpp(pvrtc, *rgb);
            PvrTcDecoder::DecodeRgb4Bpp(data, bitmap->GetSize(), pvrtc);
        }
        else {
            RgbaBitmap *rgb = static_cast<RgbaBitmap *>(bitmap);
            ColorRgba<unsigned char> *data = rgb->GetData();
            PvrTcEncoder::EncodeRgba4Bpp(pvrtc, *rgb);
            PvrTcDecoder::DecodeRgba4Bpp(data, bitmap->GetSize(), pvrtc);
        }
		write_png_file(((char*)file_name_write.data()), bitmap);
	}
	else{
		std::cout << "It needs file name for read, file name for write out";
	}
	return 0;
}
