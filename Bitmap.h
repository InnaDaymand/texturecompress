#pragma once

#include "Point2.h"
#include <png.h>
#include <pngconf.h>

namespace Javelin {

class Bitmap {
public:
    int width;
    int height;
    unsigned char *data;


    png_byte color_type;
    png_byte bit_depth;

    int number_of_passes;

    int bytesPerPixel;


    Bitmap(int w, int h, int bytesPerPixelIn)
        : width(w)
        , height(h)
        , data(new unsigned char[width * height * bytesPerPixelIn]) {
    	color_type=0;
		bit_depth=0;
		number_of_passes=0;
		bytesPerPixel=bytesPerPixelIn;
    }

    virtual ~Bitmap() {
        delete [] data;
    }

    Point2<int> GetSize() const { return Point2<int>(width, height); }

    int GetArea() const { return width * height; }

    int GetBitmapWidth() const { return width; }

    int GetBitmapHeight() const { return height; }

    const unsigned char *GetRawData() const { return data; }


};

}
