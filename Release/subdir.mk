################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../BitScale.cpp \
../MortonTable.cpp \
../PVRPNGCompress.cpp \
../PvrTcDecoder.cpp \
../PvrTcEncoder.cpp \
../PvrTcPacket.cpp 

OBJS += \
./BitScale.o \
./MortonTable.o \
./PVRPNGCompress.o \
./PvrTcDecoder.o \
./PvrTcEncoder.o \
./PvrTcPacket.o 

CPP_DEPS += \
./BitScale.d \
./MortonTable.d \
./PVRPNGCompress.d \
./PvrTcDecoder.d \
./PvrTcEncoder.d \
./PvrTcPacket.d 


# Each subdirectory must supply rules for building sources it contributes
%.o: ../%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


